import os
from fabric.api import (settings, run, sudo, reboot, cd, get, put, prefix,
                        task, local)
from fabric.contrib.files import sed, exists, append
from crypt import crypt
from config import *


@task
def full_deploy(install_znc="yes", install_tirolbot="yes"):
    """
    default is fab full_deploy:install_znc=yes,install_tirolbot=yes
    """
    if install_znc == "yes":
        prepare(znc="yes")
    else:
        prepare(znc="no")
    maintenance(update="yes",
                reboot="yes",
                tirolbot_backup="no",
                znc_backup="no")
    if install_tirolbot == "yes":
        tirolbot(run="no")


###############################################################################


@task
def prepare(znc="yes"):
    """
    Adds user, fixes ssh, secures the machine, installs needed packages, \
installs znc or use fab prepare:znc=no
    """
    prepare_adduser()
    prepare_changepassword()
    prepare_addusertosudoers()
    prepare_ssh()
    prepare_remove_backdoors()
    prepare_authorized_keys()
    prepare_fail2ban()
    prepare_iptables()
    prepare_activate_firewall_on_startup()
    prepare_packages()
    if znc == "yes":
        prepare_znc()


def prepare_adduser():
    """
    adds a new user with a disabled password
    """
    user = env.user
    with settings(user=initial_user,
                  password=initial_password,
                  port=initial_port):
        run("adduser --gecos '' --disabled-password {user}".format(user=user))


def prepare_changepassword():
    """
    Changes the password for the user
    """
    password = env.password
    user = env.user
    crypted_password = crypt(password, salt)
    with settings(user=initial_user,
                  password=initial_password,
                  port=initial_port):
        run("usermod -p {crypted_password} {user}".format(
                                            crypted_password=crypted_password,
                                            user=user))


def prepare_addusertosudoers():
    """
    adds the user to the sudoers list
    """
    user = env.user
    with settings(user=initial_user,
                  password=initial_password,
                  port=initial_port):
        run("usermod -a -G sudo {user}".format(user=user))


def prepare_ssh():
    """
    Changes the port, disables root login and restarts the ssh service
    """
    port = env.port
    with settings(port=initial_port):
        sshd_config = "/etc/ssh/sshd_config"
        sed(sshd_config, "Port 22", "Port {port}".format(port=port),
            use_sudo=True)
        sed(sshd_config, "PermitRootLogin yes", "PermitRootLogin no",
            use_sudo=True)
        sudo("service ssh restart")


def prepare_remove_backdoors():
    """
    Removes the ovh backdoor /root/.ssh/authorized_keys2
    """
    sudo("rm -f /root/.ssh/authorized_keys2")
    sudo("ls -la /root/.ssh/")


@task
def prepare_authorized_keys():
    """
    Sets up authorized_key login, requires private and pub keys on local
    """
    authorized_keys = ".ssh/authorized_keys2"
    if not exists(".ssh"):
        run("mkdir .ssh")
    if not exists(".ssh/authorized_keys2"):
        run("touch .ssh/authorized_keys2")
    with open("/home/martin/.ssh/id_rsa.pub") as f:
        public_key = f.read().strip()
    append(authorized_keys, public_key)
    sudo("chown -R {user}:{user} .ssh".format(user=env.user))
    sudo("chmod 700 .ssh")
    sudo("chmod 600 .ssh/authorized_keys2")


def prepare_fail2ban():
    """
    Installs and sets up fail2ban
    """
    sudo("apt-get install -y fail2ban")
    if exists(fail2ban_conf_location, use_sudo=True):
        sudo("rm -f {filename}".format(filename=fail2ban_conf_location))
    sudo("touch {filename}".format(filename=fail2ban_conf_location))
    append(fail2ban_conf_location, fail2ban_conf, use_sudo=True)
    sudo("service fail2ban restart")


def prepare_iptables():
    """
    Sets up the iptables firewall according to the firewall_rules
    """
    sudo("iptables -L -v")
    if exists(firewall_rules_location, use_sudo=True):
        sudo("rm -f {filename}".format(filename=firewall_rules_location))
    sudo("touch {filename}".format(filename=firewall_rules_location))
    append(firewall_rules_location, firewall_rules, use_sudo=True)
    sudo("iptables-restore < {filename}".format(
                                            filename=firewall_rules_location))
    sudo("iptables -L -v")


def prepare_activate_firewall_on_startup():
    """
    Activates the firewall on system startup
    """
    if exists(startup_script_location, use_sudo=True):
        sudo("rm -f {filename}".format(filename=startup_script_location))
    sudo("touch {filename}".format(filename=startup_script_location))
    append(startup_script_location, startup_script, use_sudo=True)
    sudo("chmod +x {filename}".format(filename=startup_script_location))


def prepare_packages():
    """
    Installs packages specified in packages_to_install from conf.py
    """
    sudo("apt-get install -y {packages}".format(packages=" ".join(
                                                        packages_to_install)))


@task
def prepare_znc():
    """
    Uploads the znc config and untars it to .znc
    """
    put("znc.tar.gz")
    run("tar xvfz znc.tar.gz")
    run("rm znc.tar.gz")


###############################################################################


@task
def maintenance(update="yes",
                reboot="no",
                tirolbot_backup="no",
                znc_backup="no"):
    """
    default is fab maintenance:update=yes,reboot=no,tirolbot_backup=no,\
znc_backup=no
    """
    if update == "yes":
        maintenance_update()
    if reboot == "yes":
        maintenance_reboot()
        maintenance_check_iptables()
    if tirolbot_backup == "yes":
        maintenance_tirolbot_backup()
    if znc_backup == "yes":
        maintenance_znc_backup()


@task
def maintenance_update():
    """
    Runs sudo apt-get -y update and apt-get -y upgrade
    """
    sudo("apt-get -y update")
    sudo("apt-get -y upgrade")


@task
def maintenance_reboot():
    """
    Reboots the machine and waits 3 minutes (180s) before reconnecting
    """
    reboot(wait=180)


@task
def maintenance_check_iptables():
    """
    Checks if iptable rules are all there
    """
    sudo("iptables -L -v")


@task
def maintenance_tirolbot_backup():
    """
    Copies the cfg, db and log(s) from the host to the local machine
    """
    local_dir = "/home/martin/workspace/tirolbot/tirolbot/remote_backup/"

    config = "config.cfg"
    db = "tirolbot.db"
    log = "tirolbot.log"

    with cd("workspace/tirolbot/tirolbot/"):
        get(config, local_dir)
        get(db, local_dir)
        get(log, local_dir)
        for i in range(1, 11):
            backup_log = "{log}.{i}".format(log=log, i=i)
            if exists(backup_log):
                get(backup_log, local_dir)
            else:
                break
        local("nautilus "
              "/home/martin/workspace/tirolbot/tirolbot/remote_backup/")


@task
def maintenance_znc_backup():
    """
    Backs up the znc config
    """
    run("tar cfz znc.tar.gz .znc")
    get("znc.tar.gz", "znc.tar.gz")
    run("rm znc.tar.gz")
    local("nautilus /home/martin/workspace/fab/fabfile/")


###############################################################################


@task
def tirolbot(run="no"):
    """
    Deploys tirolbot, use fab tirolbot:run=yes to deploy and then run the bot
    """
    tirolbot_setup_virtualenv()
    tirolbot_clone_repo()
    tirolbot_pip_install_requirements()
    tirolbot_put_files()
    tirolbot_change_sh_path()
    tirolbot_setup_cronetab()
    if run == "yes":
        tirolbot_run()


def tirolbot_setup_virtualenv():
    """
    Sets up a python3 virtualenv called tirolbot
    """
    if not exists("workspace"):
        run("mkdir workspace")
        sudo("chown -R {user}:{user} workspace".format(user=env.user))
        sudo("chmod 700 workspace")
    with cd("workspace/"):
        run("virtualenv --python=python3.2 tirolbot")


def tirolbot_clone_repo():
    """
    Clones the tirolbot repository from github.com/martinfischer/tirolbot
    """
    with cd("workspace/tirolbot/"):
        run("git clone https://github.com/martinfischer/tirolbot.git")


def tirolbot_pip_install_requirements():
    """
    Runs a pip install of the requirements from tirolbot/requirements.txt
    """
    with cd("workspace/tirolbot/"):
        with prefix("source bin/activate"):
            run("pip install -r tirolbot/requirements.txt")
            run("deactivate")


def tirolbot_put_files():
    """
    Copies the cfg, db and log(s) from the local machine to the host
    """
    local_dir = "/home/martin/workspace/tirolbot/tirolbot/"

    config = "config.cfg"
    local_config = os.path.join(local_dir, config)

    db = "tirolbot.db"
    local_db = os.path.join(local_dir, db)

    log = "tirolbot.log"
    local_log = os.path.join(local_dir, log)

    with cd("workspace/tirolbot/tirolbot/"):
        put(local_config, config)
        put(local_db, db)
        put(local_log, log)
        for i in range(1, 11):
            backup_log = "{local_log}.{i}".format(local_log=local_log, i=i)
            if os.path.exists(backup_log):
                put(backup_log, "{log}.{i}".format(log=log, i=i))


def tirolbot_change_sh_path():
    """
    Changes the path in tirolbot.sh to point to the right user directory
    """
    before = "cd /home/martin/workspace/tirolbot"
    after = "cd /home/{user}/workspace/tirolbot".format(user=env.user)
    filename = "/home/{user}/workspace/tirolbot/tirolbot/tirolbot.sh".format(
                                                                user=env.user)
    sed(filename, before, after)
    run("cat {filename}".format(filename=filename))


def tirolbot_setup_cronetab():
    """
    Sets up a crontab to run tirolbot.sh hourly
    """
    tempcron = "/tmp/crondump"
    sh = "/home/{user}/workspace/tirolbot/tirolbot/tirolbot.sh".format(
                                                                user=env.user)
    if exists(tempcron):
        run("rm -f {tempcron}".format(tempcron=tempcron))
    with settings(warn_only=True):
        run("crontab -l > {tempcron}".format(tempcron=tempcron))
    append(tempcron, "@hourly {sh}".format(sh=sh))
    run("crontab /tmp/crondump")
    run("crontab -l")
    run("rm -f {tempcron}".format(tempcron=tempcron))


@task
def tirolbot_run():
    """
    Runs the tirolbot with shell logging on
    """
    sh = "/home/{user}/workspace/tirolbot/tirolbot/tirolbot.sh".format(
                                                                user=env.user)
    run(sh)


###############################################################################


@task
def remote_run(command):
    """
    Usage: fab remote_run:"command"
    """
    return run(command)


@task
def remote_sudo(command):
    """
    Usage: fab remote_sudo:"command"
    """
    return sudo(command)
