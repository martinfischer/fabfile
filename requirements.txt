Fabric==1.4.3
argparse==1.2.1
distribute==0.6.24
pycrypto==2.6
ssh==1.7.14
wsgiref==0.1.2
